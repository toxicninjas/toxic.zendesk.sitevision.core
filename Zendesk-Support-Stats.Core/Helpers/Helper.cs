﻿using Microsoft.Extensions.Caching.Memory;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Zendesk_Support_Stats.Core.Models;

/* NOTE */
/* request1 refers to todays values, request2 refers to (yesterdays/last week/last month) */


namespace Zendesk_Support_Stats.Core.Helpers
{
    public static class Helper
    {
        private static MemoryCache _cache = new MemoryCache(new MemoryCacheOptions());
        private const int cacheTimer = 60;
        public static RestRequest GenerateRequest()
        {
            var request = new RestRequest(Method.GET);

            request.AddHeader("Authorization", "Basic a2FsbGUuZWtzdHJhbmRAdG94aWMuc2UvdG9rZW46U3p5WENRVVloTXp4OU9SYU13eDYzamdWV2ZzUnlBdTEyTHRiN1Y5UQ==");
            request.AddHeader("SzyXCQUYhMzx9ORaMwx63jgVWfsRyAu12Ltb7V9Q", "kalle.ekstrand@toxic.se");
            request.AddHeader("Content-Type", "application/json");
            request.AddHeader("Cookie", "__cfduid=dd88c8940e90124b8ea04605ca0b49fbf1615881067; __cfruid=665d29330b68685faef23479733ec6a4d8d08ea3-1615881068; _zendesk_session=BAh7CEkiD3Nlc3Npb25faWQGOgZFVEkiJTY4OTAxNDVhODA0N2Q1YTg5ODRmN2UxYzU3ZmJjNGJkBjsAVEkiDGFjY291bnQGOwBGaQMF7A5JIgpyb3V0ZQY7AEZpA3x6DQ%3D%3D--c0146cc1ec9af8e01d4185c59784f2dcb7c8fbba");
            request.AddParameter("application/json", "[\r\n    {\r\n        \"EmployeeId\": 55,\r\n        \"EditionId\": 82,\r\n        \"EmailAddress\": \"anette.wold-ylenstrand@toxic.se\"\r\n    }\r\n]", ParameterType.GetOrPost);

            if(request != null)
            {
                return request;
            }

            return null;
        }

        public static ReturnEntity GetSolvedTicketsThisMonth()
        {
            var cacheKey = "solvedTicketsThisMonth";
            var result = GetCache<ReturnEntity>(cacheKey);
            if (result != null) return result;

            var year = DateTime.Today.Year;
            var currentMonth = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1).ToShortDateString();
            var lastMonth = new DateTime(DateTime.Today.Year, DateTime.Today.AddMonths(-1).Month, 1).ToShortDateString();

            var currentDay = DateTime.Today.ToShortDateString();

            var lastMonthInt = DateTime.Today.AddMonths(-1).Month;
            var lastDayOfMonth = DateTime.DaysInMonth(year, lastMonthInt);

            var lastDay = new DateTime(DateTime.Today.Year, lastMonthInt, lastDayOfMonth).ToShortDateString();

            var currentClient = new RestClient($"https://toxic.zendesk.com/api/v2/search.json?query=type:ticket solved>{currentMonth} solved<{currentDay}");
            var lastMonthClient = new RestClient($"https://toxic.zendesk.com/api/v2/search.json?query=type:ticket solved>{lastMonth} solved<{lastDay}");

            var request1 = GenerateRequest();
            var request2 = GenerateRequest();

            var response1 = currentClient.Execute(request1);
            var response2 = lastMonthClient.Execute(request2);

            var currentModel = Deserialize(response1.Content);
            var lastModel = Deserialize(response2.Content);

            if (currentModel != null && lastModel != null)
            {
                var returnEntity = CreateModel(currentModel, lastModel);

                if (returnEntity != null)
                {
                    var returnModel = SetCache(cacheKey, returnEntity);

                    return returnModel != null ? returnModel : null;
                }
            }

            return null;
        }

        public static ReturnEntity GetSolvedTicketsThisWeek()
        {
            var cacheKey = "solvedTicketsThisWeek";
            var result = GetCache<ReturnEntity>(cacheKey);
            if (result != null) return result;

            var currentWeek = DateTime.Today.AddDays(-(int)DateTime.Today.DayOfWeek + (int)DayOfWeek.Monday).ToShortDateString();
            var lastWeek = DateTime.Today.AddDays(-(int)DateTime.Today.DayOfWeek + (int)DayOfWeek.Monday).AddDays(-7).ToShortDateString();
            var fridayLastWeek = DateTime.Today.AddDays(-(int)DateTime.Today.DayOfWeek + (int)DayOfWeek.Monday).AddDays(-7).AddDays(4).ToShortDateString();

            var currentDay = DateTime.Today.ToShortDateString();

            var currentClient = new RestClient($"https://toxic.zendesk.com/api/v2/search.json?query=type:ticket solved>{currentWeek} solved<{currentDay}");
            var lastClient = new RestClient($"https://toxic.zendesk.com/api/v2/search.json?query=type:ticket solved>{lastWeek} solved<{fridayLastWeek}");

            var request1 = GenerateRequest();
            var request2 = GenerateRequest();


            var response1 = currentClient.Execute(request1);
            var response2 = lastClient.Execute(request2);


            var currentModel = Deserialize(response1.Content);
            var lastModel = Deserialize(response2.Content);


            if (currentModel != null && lastModel != null)
            {
                var returnEntity = CreateModel(currentModel, lastModel);

                if (returnEntity != null)
                {
                    var returnModel = SetCache(cacheKey, returnEntity);

                    return returnModel != null ? returnModel : null;
                }
            }

            return null;
        }

        public static ReturnEntity GetSolvedTicketsToday()
        {
            var cacheKey = "solvedTickets";
            var result = GetCache<ReturnEntity>(cacheKey);
            if (result != null) return result;

            var currentDay = DateTime.Today.ToShortDateString();
            var lastDay = DateTime.Today.AddDays(-1).ToShortDateString();

            var currentClient = new RestClient($"https://toxic.zendesk.com/api/v2/search.json?query=type:ticket solved:{currentDay}");
            var lastClient = new RestClient($"https://toxic.zendesk.com/api/v2/search.json?query=type:ticket solved:{lastDay}");

            var request1 = GenerateRequest();
            var request2 = GenerateRequest();

            var response1 = currentClient.Execute(request1);
            var response2 = lastClient.Execute(request2);


            var currentModel = Deserialize(response1.Content);
            var lastModel = Deserialize(response2.Content);


            if (currentModel != null && lastModel != null)
            {
                var returnEntity = CreateModel(currentModel, lastModel);

                if (returnEntity != null)
                {
                    var returnModel = SetCache(cacheKey, returnEntity);

                    return returnModel != null ? returnModel : null;
                }
            }

            return null;
        }

        public static ReturnEntity GetNewTicketsToday()
        {
            var cacheKey = "newTickets";
            var result = GetCache<ReturnEntity>(cacheKey);
            if (result != null) return result;

            var currentDay = DateTime.Today.ToShortDateString();
            var yesterday = DateTime.Today.AddDays(-1);


            var currentClient = new RestClient($"https://toxic.zendesk.com/api/v2/search.json?query=type:ticket created:{currentDay}");
            var yesterdayClient = new RestClient($"https://toxic.zendesk.com/api/v2/search.json?query=type:ticket created:{yesterday}");

            var request1 = GenerateRequest();
            var request2 = GenerateRequest();

            var response1 = currentClient.Execute(request1);
            var response2 = yesterdayClient.Execute(request2);

            var currentModel = Deserialize(response1.Content);
            var lastModel = Deserialize(response2.Content);

            if (currentModel != null && lastModel != null)
            {
                var returnEntity = CreateModel(currentModel, lastModel);

                if (returnEntity != null)
                {
                    var returnModel = SetCache(cacheKey, returnEntity);

                    return returnModel != null ? returnModel : null;
                }
            }

            return null;
        }

        public static int GetUnsolvedTicketsCount()
        {
            var cacheKey = "unsolvedTickets";
            var result = GetCache<SupportCase>(cacheKey);
            if (result != null) return result.count;

            var client = new RestClient("https://toxic.zendesk.com/api/v2/search.json?query=status<solved type:ticket");

            var request = GenerateRequest();
            var response = client.Execute(request);
            var model = Deserialize(response.Content);

            result = SetCache(cacheKey, model);

            return result != null ? result.count : 0;
        }

        public static int GetAllOpenTickets()
        {
            var cacheKey = "openTickets";
            var result = GetCache<SupportCase>(cacheKey);
            if (result != null) return result.count;

            var client = new RestClient("https://toxic.zendesk.com/api/v2/search.json?query=type:ticket status:open");
            client.Timeout = -1;

            var request = GenerateRequest();
            var response = client.Execute(request);
            var model = Deserialize(response.Content);

            result = SetCache(cacheKey, model);

            if (result != null)
            {
                return result.count;
            }

            return 0;
        }

        public static SupportCase Deserialize(string rawJson)
        {
            var obj = JsonConvert.DeserializeObject<SupportCase>(rawJson);

            return obj != null ? obj : null;
        }

        private static T GetCache<T>(string cacheKey)
        {
            T data;
            _cache.TryGetValue(cacheKey, out data);
          
            return data;
        }

        private static T SetCache<T>(string cacheKey, T data)
        {
            if (data != null)
            {
                var cacheEntryOptions = new MemoryCacheEntryOptions()
                    .SetPriority(CacheItemPriority.Low)
                    .SetAbsoluteExpiration(TimeSpan.FromMinutes(cacheTimer));
                _cache.Set(cacheKey, data, cacheEntryOptions);
            }

            return data;
        }

        private static ReturnEntity CreateModel(SupportCase currentModel, SupportCase lastModel)
        {
            var returnEntity = new ReturnEntity();

            returnEntity.CurrentCount = currentModel.count;
            returnEntity.LastCount = lastModel.count;
            returnEntity.DiffCount = returnEntity.CurrentCount - returnEntity.LastCount;

            return returnEntity != null ? returnEntity : null;
        }
    }
}
