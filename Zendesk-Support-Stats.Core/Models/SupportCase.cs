﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Zendesk_Support_Stats.Core
{

    public class SupportCase
    {
        public Ticket_Metrics[] ticket_metrics { get; set; }
        public string next_page { get; set; }
        public object previous_page { get; set; }
        public int count { get; set; }
    }

    public class Ticket_Metrics
    {
        public string url { get; set; }
        public long id { get; set; }
        public int ticket_id { get; set; }
        public DateTime created_at { get; set; }
        public DateTime updated_at { get; set; }
        public int group_stations { get; set; }
        public int assignee_stations { get; set; }
        public int reopens { get; set; }
        public int replies { get; set; }
        public DateTime? assignee_updated_at { get; set; }
        public DateTime requester_updated_at { get; set; }
        public DateTime status_updated_at { get; set; }
        public DateTime? initially_assigned_at { get; set; }
        public DateTime? assigned_at { get; set; }
        public DateTime? solved_at { get; set; }
        public DateTime latest_comment_added_at { get; set; }
        public Reply_Time_In_Minutes reply_time_in_minutes { get; set; }
        public First_Resolution_Time_In_Minutes first_resolution_time_in_minutes { get; set; }
        public Full_Resolution_Time_In_Minutes full_resolution_time_in_minutes { get; set; }
        public Agent_Wait_Time_In_Minutes agent_wait_time_in_minutes { get; set; }
        public Requester_Wait_Time_In_Minutes requester_wait_time_in_minutes { get; set; }
        public On_Hold_Time_In_Minutes on_hold_time_in_minutes { get; set; }
    }

    public class Reply_Time_In_Minutes
    {
        public int? calendar { get; set; }
        public int? business { get; set; }
    }

    public class First_Resolution_Time_In_Minutes
    {
        public int? calendar { get; set; }
        public int? business { get; set; }
    }

    public class Full_Resolution_Time_In_Minutes
    {
        public int? calendar { get; set; }
        public int? business { get; set; }
    }

    public class Agent_Wait_Time_In_Minutes
    {
        public int? calendar { get; set; }
        public int? business { get; set; }
    }

    public class Requester_Wait_Time_In_Minutes
    {
        public int? calendar { get; set; }
        public int? business { get; set; }
    }

    public class On_Hold_Time_In_Minutes
    {
        public int calendar { get; set; }
        public int business { get; set; }
    }

}
