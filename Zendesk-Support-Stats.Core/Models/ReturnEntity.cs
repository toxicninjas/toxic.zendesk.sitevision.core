﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Zendesk_Support_Stats.Core.Models
{
    public class ReturnEntity
    {
        public int CurrentCount { get; set; }
        public int LastCount { get; set; }
        public int DiffCount { get; set; }
    }
}
