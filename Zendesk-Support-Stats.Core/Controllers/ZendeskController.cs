﻿using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Zendesk_Support_Stats.Core.Helpers;

namespace Zendesk_Support_Stats.Core.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class ZendeskController : ControllerBase
    {
        [Route("opentickets")]
        [HttpGet]
        public IActionResult GetOpenTickets()
        {
            return Json(Helper.GetAllOpenTickets());
        }

        [Route("newtoday")]
        public IActionResult NewTicketsToday()
        {
            return Json(Helper.GetNewTicketsToday());
        }

        [Route("solvedtoday")]
        public IActionResult SolvedTicketsToday()
        {
            return Json(Helper.GetSolvedTicketsToday());
        }

        [Route("solvedthisweek")]
        public IActionResult SolvedTicketsThisWeek()
        {
            return Json(Helper.GetSolvedTicketsThisWeek());
        }

        [Route("solvedthismonth")]
        public IActionResult SolvedTicketsThisMonth()
        {
            return Json(Helper.GetSolvedTicketsThisMonth());
        }

        [Route("unsolvedtickets")]
        public IActionResult GetUnsolvedTicketsCount()
        {
            return Json(Helper.GetUnsolvedTicketsCount());
        }

        protected JsonResult Json(object obj)
        {
            return new JsonResult(new { Result = obj });
        }
    }
}
